package com.demo.autestproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuTestProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuTestProjectApplication.class, args);
    }

}
