FROM openjdk:11
EXPOSE 8080
ADD target/au-test-project.jar au-test-project.jar
ENTRYPOINT ["java","-jar","/au-test-project.jar"]
